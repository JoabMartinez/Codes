import pygame
import sys
from pygame.locals import *
from transitions import Machine
import random

# Fps
FPS = 30
fpsClock = pygame.time.Clock()

# Window
WIDTH=1024
HEIGHT=720
DISPLAYSURF = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Helicopter game')

# image
skyColor = (168, 255, 255)
moveSpeed=5

# Speed
y_speed = 1

# Position
y_coord = 10

#Color
black = (0, 0, 0)



class Player(pygame.sprite.Sprite):

    states = ['start', 'fly', 'die']

    def __init__(self, name):
        super().__init__()
        self.image = pygame.image.load('Heli.png')
        self.rect = self.image.get_rect()
        self.name = name
        self.machine = Machine(model=self, states=Player.states, initial='flying')
        self.machine.add_transition(trigger='fly', source='start', dest='shoot')
	

    def update(self):
        keys = pygame.key.get_pressed()
        if keys[K_UP]:
            self.rect.y -= 5
            if self.rect.y < 0:
                print("too far up")
                self.rect.y = 0
        if keys[K_DOWN]:
            self.rect.y += 5
            if self.rect.y > HEIGHT - 100:
                print("too far down")
                self.rect.y = HEIGHT - 100


class Enemy(pygame.sprite.Sprite):

    def __init__(self, color):
        super().__init__()

        self.image = pygame.Surface([30, 30])
        self.image.fill(color)

        self.rect = self.image.get_rect()


	
class Rocket(pygame.sprite.Sprite):

    states = ['shoot', 'fly', 'die']

    def __init__(self):
        super().__init__()

        self.image = pygame.Surface([20, 20])
        self.rect = self.image.get_rect()
        self.machine = Machine(model=self, states=Rocket.states, initial='flying')
        self.machine.add_transition(trigger='shoot', source='fly', dest='die')
		

    def update(self):
        self.rect.x += 10


pygame.init()

player = Player("Heli")
# player_list=pygame.sprite.Group()
rocket_list = pygame.sprite.Group()
all_sprites_list = pygame.sprite.Group()
enemy_list = pygame.sprite.Group()
lastTick = pygame.time.get_ticks()

# create blocks
for i in range(50):
    enemy = Enemy(black)

    enemy.rect.x = random.randrange(WIDTH/2,WIDTH-50)
    enemy.rect.y = random.randrange(HEIGHT-50)

    enemy_list.add(enemy)
    all_sprites_list.add(enemy)

score = 0

while True:  # the main game loop

    DISPLAYSURF.fill(skyColor)

    player.update()
    all_sprites_list.add(player)

    if pygame.time.get_ticks() >= lastTick + 15000:
        pygame.quit()
        sys.exit()
	

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

        if event.type == pygame.MOUSEBUTTONDOWN:
            rocket = Rocket()
            # rocket.rect.x = player.rect.x
            rocket.rect.y = player.rect.y
            all_sprites_list.add(rocket)
            rocket_list.add(rocket)

    all_sprites_list.update()

    for rocket in rocket_list:
         enemy_hit_list = pygame.sprite.spritecollide(rocket, enemy_list, True)

         for enemy in enemy_hit_list:
            rocket_list.remove(rocket)
            all_sprites_list.remove(rocket)
            score += 1
            print(score)

         if rocket.rect.y < -10:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)



    all_sprites_list.draw(DISPLAYSURF)
    pygame.display.update()
    fpsClock.tick(FPS)
